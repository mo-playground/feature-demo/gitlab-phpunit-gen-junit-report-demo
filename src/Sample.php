<?php

namespace Mouson\Template;

class Sample
{
    public function isTrue(): bool
    {
        return true;
    }

    public function not(bool $bool): bool
    {
        return !$bool;
    }
}
